source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby File.read(File.join(__dir__, ".ruby-version"), mode: "rb").chomp

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem "rails", "~> 7.0"
# # We don't use sprockets but rails has it as dependecy...
# # And Sprockets 4 breaks rails startup when we don't use it.
# gem "sprockets", "~> 3", require: false
# JSON API Serializers
gem "fast_jsonapi"
# Use postgresql as the database for Active Record
gem "pg", ">= 0.18", "< 2.0"
# Use PG full-text search helpers
gem "pg_search"
# Use Puma as the app server
gem "puma", "~> 5"
gem "react-rails", "~> 2.4"

# Transpile app-like JavaScript. Read more: https://github.com/shakacode/shakapacker/
gem "shakapacker", "6.0.0.rc.13"

# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem "turbolinks", "~> 5"
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", ">= 1.4.1", require: false

gem "workflow-activerecord", "~> 4.1"

# Password-less authentication (via Magic Links)
# TODO Change back to stable version once > v0.9.0 is out
gem "passwordless", "> 0.9.0"
# Authorization
gem "cancancan"
# Because Ruby & Rails 7 are not happy without the explicit gem
gem "net-http"
# CF https://github.com/rubygems/rubygems/issues/5016
gem "uri", ">= 0.11.0"

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem "pry", platforms: [:mri, :mingw, :x64_mingw]
  gem "standard", require: false
  gem "rubocop-rails", require: false
  gem "rubocop-minitest", require: false
  gem "ruby-graphviz"
  gem "brakeman"
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem "web-console", ">= 3.3.0"
  gem "listen", ">= 3.0.5", "< 3.2"
  gem "scss_lint", require: false
  # Move back to versioning once 0.9.0 has been released
  gem "bundler-audit", github: "rubysec/bundler-audit", ref: "21f8913b521f297792d5c1c500612134fabe4669"
  # Detect N+1 queries
  gem "bullet"
  # Profiler
  # First usage: bundle exec rails g rack_profiler:install
  gem "rack-mini-profiler", require: false
  # Annotate models with comments about the table schema
  gem "annotate"
end

group :test do
  gem "minitest"
  gem "minitest-reporters"
  gem "factory_bot_rails"
  # Need a JS runtime for ExecJS
  # https://github.com/rails/execjs
  # gem "therubyracer"
  gem "mini_racer"
  # Adds support for Capybara system testing and selenium driver
  gem "capybara", ">= 2.15"
  gem "selenium-webdriver"
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem "chromedriver-helper"
  gem "vcr"
  gem "webmock"
  gem "simplecov", require: false
  gem "simplecov-cobertura", require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: [:mingw, :mswin, :x64_mingw, :jruby]

gem "delayed_job_active_record"
gem "daemon"
