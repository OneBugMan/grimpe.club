-include env.mk
export
ifeq (test, $(findstring test, $(MAKECMDGOALS)))
-include env.test.mk
export
endif

SHELL:=/usr/bin/env bash

.env.sh:
	touch $@ && chmod +x $@

.env.test.sh:
	touch $@ && chmod +x $@

env.mk: .env.sh
	./.env.sh | sed 's/"//g ; s/=/:=/' > $@

env.test.mk: .env.test.sh
	./.env.test.sh | sed 's/"//g ; s/=/:=/' > $@

.PHONY: ruby
ruby: .ruby-version
	@echo "Running in $(RAILS_ENV) environment"
	ruby -v

.PHONY: install
install: ruby ## Install development dependencies
	bundle config set path 'vendor'
	bundle install -j $$(nproc)

.PHONY: db
db: ## Create the database
	bundle exec rake db:exists && bundle exec rake db:migrate || bundle exec rake db:setup

.PHONY: assets-precompile
assets-precompile:
	bundle exec rake assets:precompile

.PHONY: db-seed
db-seed: db confirm ## Seed the database with test data
	bundle exec rake db:seed

.PHONY: db-drop
db-drop: confirm ## Drop the database
	bundle exec rails db:environment:set
	bundle exec rake db:drop

.PHONY: confirm
confirm: valid-env
	@( if [ "$${RAILS_ENV}" != "test" ]; then echo -n "(env:$$RAILS_ENV) Are you sure? [y/N] "; read -r sure && case "$$sure" in [yY]) true;; *) false;; esac; fi )

.PHONY: valid-env
valid-env:
	@( [ -n "$$RAILS_ENV" ] || ( \
	     echo "RAILS_ENV variable is not defined!" && \
	     echo "Please verify your local .env file." && exit 1 ) )

.PHONY: routes
routes: ## Display available routes from the web service
	bundle exec rails routes

.PHONY: run
run: install frontend db server ## Run the development server

.PHONY: server
server: ## Launch the application server
	bundle exec rails s

.PHONY: worker
worker: ## Launch the worker for processing background jobs
	bundle exec rake jobs:work

.PHONY: console
console: install db ## Launch a rails console
	bundle exec rails c

.PHONY: lint
lint: install ## Lint ruby codebase
	bundle exec rubocop

test/reports/:
	mkdir -p test/reports/

.PHONY: audit
audit: test/reports/ install ## Check dependencies known security vulnerabilities
	bundle exec bundle-audit check --update --format junit -o test/reports/bundle-audit-results.junit
	./scripts/yarn-audit.sh

.PHONT: security-checks
security-checks: test/reports/ audit ## Static analysis of potential security vulnerabilities
	bundle exec brakeman -o test/reports/brakeman-results.html -o test/reports/brakeman-results.junit

.PHONY: lint-fix
lint-fix: frontend
	bundle exec standardrb --fix

.PHONY: eslint-fix
eslint-fix: frontend
	npm run lint-fix

.PHONY: frontend
frontend: install
	yarn install --check-files
	bundle exec rails webpacker:compile

.PHONY: test
test: install db-drop db
	NODE_ENV=test bundle exec rails webpacker:compile
	bundle exec rails test

.PHONY: single-test
single-test:
	bundle exec rails test $(filter-out $@, $(MAKECMDGOALS))

.PHONY: clean-env
clean-env:
	rm -rf env.mk env.test.mk

.PHONY: test-in-docker
test-in-docker:
	docker-compose exec app make test

.PHONY: db-seed-in-docker
db-seed-in-docker:
	docker-compose exec app make db-seed

.PHONY: run-in-docker
run-in-docker:
	docker-compose up --build

.PHONY: clean-docker
clean-docker: clean-env
	docker-compose down

#############################
# Rails Development process #
#############################

.PHONY: db-generate-model
db-generate-model:
	bundle exec rails generate model Setting s_key:string s_value:binary
	bundle exec rails generate model Member first_name:string last_name:string email:string dob:date sexe:integer address:text zipcode:string city:string phone:string workflow_state:string member:references
	bundle exec rails generate model Group name:string description:string
	bundle exec rails generate model Membership member:references group:references year:integer


.PHONY: model-generate-serializers
model-generate-serializers:
	bundle exec rails generate serializer Member


.PHONY: install-react
install-react:
	bundle exec rails webpacker:install:react
	bundle exec rails generate react:install

%:
	@echo "You called an empty target “make $@”. Please retry."
	@$(MAKE) help

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' -H $(MAKEFILE_LIST) \
		| awk 'BEGIN {FS = ":"}; {printf "%s:%s\n", $$2, $$3, $$1}' \
		| sort \
		| awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-35s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
