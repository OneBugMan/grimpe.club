# grimpe.club - Gérer son club dʼEscalade facilement

> ⚠️ ⚠️ ⚠️
>
> Ce projet est en cours de réalisation et l'application n'est pas
> prête pour être utilisée. Si vous voulez participer, n'hésitez pas à
> vous inscrire sur la mailing liste [« outils
> web »](https://listes.montagne.fsgt.org/info/outils-web) de la
> section montagne de la FSGT.
>
> ⚠️ ⚠️ ⚠️

Ce dépôt contient le code source de lʼapplication <`grimpe.club`>, une application web dédiée à la gestion de club de sports (pensée en premier lieu pour des clubs dʼescalade de la FSGT).

Lʼapplication a pour but principal dʼoffrir :

- Une interface de pré-inscription pour les personnes souhaitant rejoindre un club ;
- Une interface dʼinscription/gestion des membres. Permettant la simplification de toutes démarches administratives annuelles nécessaires pour chaque membre ;
- Un espace « membre » pour regrouper des informations liés à la gestion du club et son organisation interne.

L’initiative a été portée par des membres des clubs « Le Mur » et « Vertical 12 » en espérant attirer d’autres clubs de la FSGT pour porter un projet libre, communautaire et gratuit.

Ce projet est publié avec une licence libre de type [AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html), donnant le droit de compréhension, de modification et dʼutilisation à nʼimporte qui lisant ces quelques lignes. La seule condition exigée étant que toutes versions modifiées de cette présente application **doit** également publier son code source modifié.

## Développement

### L’interface utilisateur

- Technologies « classiques » HTML/CSS/Javascript et les pages HTML sont servis par Rails (cf. [Le Serveur](#le-serveur))
- Framework jQuery pour la partie « dynamique » (Javascript)
- Framework Bootstrap pour le style de l’interface (CSS)

Tous les fichiers du *frontend* (hormis les vues HTML) sont présentes dans le dossier `app/frontend/`. Le module rails [`webpacker`](https://github.com/rails/webpacker) permet d'utiliser webpack comme pré-processeur et générateur de paquets « tout-en-un » et permet d'inclure le résultat dans les vues HTML.

Même s'il est possible d'utiliser webpack directement pour démarrer le frontend (via l'executable `./bin/webpack-dev-server`), il est **fortement déconseillé** de le faire. À la place, démarrer Rails (cf. [Le Serveur](#le-serveur)) qui se chargera de compiler le frontend avec webpack pour vous à la demande. Les potentiels erreurs de compilations se retrouveront dans [les logs de Rails](#vérifier-les-journaux-de-logs-et-les-mails).

### Le Serveur

- Ruby 2.7+
- Framework Ruby On Rails (open-source [licence MIT](https://opensource.org/licenses/MIT))
- Base de données PostgreSQL, en version 12 recommandée (open-source [PostgreSQL Licence](https://opensource.org/licenses/postgresql))
- Hébergement éthique et responsable par Easter-Eggs ?

## Contribuer

### Traductions

L’interface est pour l’instant uniquement en Anglais. Un effort de traduction est nécessaire pour rendre l’application disponible, au moins en Français, puis dans d’autres langues éventuellement.

TODO: brancher weblate pour traduire les fichiers i18n

### Code

Pour démarrer assurez vous d’avoir un environnement avec Ruby (version dans le fichier `.ruby-version`), NodeJS 12+ et Yarn, les libs clientes de PostgreSQL ([`libpq5`](https://packages.debian.org/sid/libpq5) sous debian). Il vous faudra également un service PostgreSQL pour la base de donnée.

_Si vous préferez utiliser des conteneurs pour faire tourner l'application il y a un ficher d'orchestration utilisable avec `docker-compose`._

#### Configurer l’application

Localement vous devez configurer deux environnement : le « service web » pour démarrer l’application et les « tests » pour pouvoir lancer les tests automatiques.

Copiez les deux fichiers d’exemple comme ceci :

``` bash
cp .env.example .env.sh
cp .env.test.example .env.test.sh
```

Éditez les fichiers pour mettre votre propre configuration (notamment les informations de connections à la base de donnée par exemple).

_Si vous utilisez des conteneurs, les valeurs initiales du fichier de configuration d'exemple devraient suffire_

#### Démarrer l’application

La tache `run` prépare tout pour vous et vous devriez avoir que ça à faire :

```
make run
```

En détails, elle lance l’installation des dépendances (`make install`), les migrations de BDD (`make db`) puis lance le serveur applicatif (`make server`).

_Si vous utilisez des conteneurs vous pouvez utiliser la tache `make run-in-docker`. Attention : si vous utilisez `docker-compose` à la main il vous faudra sourcer le fichier de configuration `.env.sh`_


#### Ajouter des données de démo (optionnel)

La tache `db-seed` permet de remplir la base de donnée avec des données de demo et des utilisateurs administrateurs :

```
make db-seed
```

Vous pourrez ainsi vous connecter avec l'un des comptes administrateurs suivant : `janja@grimpe.club` ou `nina@grimpe.club` ou `martine@grimpe.club` ou `steph@grimpe.club`. Jetez un oeil aux logs applicatifs pour voir le lien de connection dans les logs de mails.

_Si vous utilisez des conteneurs vous pouvez utiliser la tache `make db-seed-in-docker`. Attention : si vous utilisez `docker-compose` à la main il vous faudra sourcer le fichier de configuration `.env.sh`_

#### Tester l’application

Lancer les tests est aussi simple que d’exécuter la tache _make_ associée :

```
make test
```

_Si vous utilisez des conteneurs vous pouvez utiliser la tache `make test-in-docker`. Attention : si vous utilisez `docker-compose` à la main il vous faudra sourcer le fichier de configuration `.env.test.sh`_

#### Vérifier les journaux de logs et les mails

Lorsque vous développez localement, les logs applicatifs sont envoyés dans le fichier `log/development.log` et les mails sont enregistrés dans le dossier `tmp/mails/`
