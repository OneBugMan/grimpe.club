class AuthenticatedController < ApplicationController
  rescue_from CanCan::AccessDenied do |e|
    Rails.logger.debug { "Access denied on #{e.action} #{e.subject.inspect}" }
    respond_to do |format|
      format.json { head :forbidden, content_type: "text/html" }
      format.html do
        flash[:warning] = e.message
        redirect_to(
          session.delete(:return_to) || root_path
        )
      end
      format.js { head :forbidden, content_type: "text/html" }
    end
  end

  # Force authentication on all routes
  before_action :require_user!
  # Force authorization on all routes
  check_authorization
  # Load resource(s) before every action
  load_resource
  # Handle authorization on resource(s) before every action
  authorize_resource

  private

  def respond_to_html_json_csv(
    entities,
    json_serializer,
    json_serializer_options: {},
    csv_columns: []
  )
    respond_to do |format|
      format.html
      format.json do
        render json: json_serializer.new(
          entities, json_serializer_options
        ).serialized_json
      end
      if csv_columns.present?
        format.csv do
          render_csv(entities, columns: csv_columns)
        end
      end
    end
  end

  def render_csv(entities, columns: nil)
    query = entities
    query = query.select(*columns) if columns.present?
    filename = entities&.last&.class&.table_name || "result"
    rows = []

    stream_query_rows(query.to_sql) do |row|
      rows << row
    end

    render csv: rows.join(""), filename: filename
  end

  def stream_query_rows(sql_query, options = "WITH CSV HEADER")
    conn = ActiveRecord::Base.connection.raw_connection
    conn.copy_data "COPY (#{sql_query}) TO STDOUT #{options}" do
      while (row = conn.get_copy_data)
        yield row
      end
    end
  end
end
