class GroupsController < AuthenticatedController
  skip_load_resource only: :show

  def index
    defaults = {limit: Setting.cached_or_find_by_key(Setting::CONFIGURABLE_LISTING_PER_PAGE), offset: 0}

    @index_params = index_params
    @limit, @offset =
      @index_params
        .with_defaults(defaults)
        .slice(:limit, :offset)
        .values
    @page = @offset.to_i / @limit + 1
    @search = @index_params[:search]
    @order = @index_params[:order]

    @groups = @groups.search(@search) if @search.present?
    @groups = @groups.order(@order.to_h) if @order
    @total = @groups.count
    @groups = @groups.limit(@limit).offset(@offset)

    respond_to_html_json_csv(
      @groups,
      V1::GroupSerializer,
      csv_columns: [:name, :description]
    )
  end

  def new
  end

  def edit
  end

  def create
    if @group.save
      flash[:info] = I18n.t("groups.new.success")

      redirect_to group_path(@group)
    else
      if @group.errors.any?
        flash[:error] = render_activerecord_errors(@group.errors)
      end

      render action: :new
    end
  end

  def show
    @group = Group.includes([:current_members]).find(params[:id])

    respond_to_html_json_csv(
      @group,
      V1::GroupSerializer
    )
  end

  def update
    if update_params.present? && @group.update(update_params)
      flash[:info] = I18n.t("groups.update.success")

      redirect_to group_path(@group)
    elsif @group.errors.any?
      flash[:error] = render_activerecord_errors(@group.errors)
      redirect_to edit_group_path(@group)
    else
      redirect_to group_path(@group)
    end
  end

  def batch
    @groups = Group.where(id: params[:group_ids])

    if @groups.present?
      @destroyed_groups = @groups.destroy_all

      flash[:warning] = I18n.t(
        "groups.batch.success",
        element: helpers.pluralize(@destroyed_groups.count, I18n.t("activerecord.models.group"))
      )

      redirect_to groups_path
    end
  end

  def import
    file = import_params
    created = 0

    CSV.foreach(file.path, headers: true) do |row|
      Group.create!(row.to_hash)
      created += 1
    end

    flash[:info] = I18n.t("groups.import.success", created: created)

    redirect_to groups_path
  rescue => e
    flash[:warning] = I18n.t("groups.import.failure", created: created, message: e.message)

    redirect_to groups_path
  end

  private

  def index_params
    params.permit(
      :search,
      :offset,
      order: [:created_at, :name]
    )
  end

  def create_params
    params.require(:group).permit(:name, :description, :email)
  end

  def update_params
    create_params
  end

  def import_params
    params.require(:import_file)
  end
end
