require "csv"

class MembersController < AuthenticatedController
  skip_before_action :require_user!, only: :register
  skip_load_resource only: :register

  def index
    authorize! :manage, Member

    load_active_members

    respond_to_html_json_csv(
      @active_members,
      V1::MemberSerializer,
      json_serializer_options: {include: [:groups]},
      csv_columns: Member::EXPORT_FIELDS
    )
  end

  def new
  end

  def edit
  end

  def create
    if @member.save
      flash[:info] = <<~INFO
        ✓ Member has been created!
      INFO

      redirect_to member_path(@member)
    else
      if @member.errors.any?
        flash[:error] = render_activerecord_errors(@member.errors)
      end

      render :new
    end
  end

  def register
    @registration = Registration.where(public: current_user.blank?).ongoing.first

    if @registration.nil?
      flash[:warning] = I18n.t("members.register.not_open")
      redirect_to root_path and return
      return
    end

    # We manually load_resource and authorize_resource
    @member = Member.new(register_params)
    authorize! :register, @member

    if @member.save
      flash[:info] = <<~INFO
        ✓ Member has been created!
      INFO
    elsif @member.errors.any?
      flash[:error] = render_activerecord_errors(@member.errors)
    end

    render "root/index"
  end

  def update
    if update_params.present? && @member.update(update_params)
      flash[:info] = I18n.t("members.update.success")

      redirect_to member_path(@member)
    else
      if @member.errors.any?
        flash[:error] = render_activerecord_errors(@member.errors)
      end

      render :edit
    end
  end

  def batch
    @members = @members.where(id: params[:ids])

    if @members.present?
      @destroyed_members = @members.destroy_all

      flash[:warning] = I18n.t(
        "members.batch.success",
        element: helpers.pluralize(@destroyed_members.count, I18n.t("activerecord.models.member"))
      )

      redirect_to members_path
    end
  end

  def import
    file = import_params
    created = 0

    CSV.foreach(file.path, headers: true) do |row|
      Member.create_from_raw!(row.to_hash)
      created += 1
    end

    flash[:info] = I18n.t("members.import.success", created: created)

    redirect_to members_path
  rescue => e
    flash[:warning] = I18n.t("members.import.failure", created: created, message: e.message)

    redirect_to members_path
  end

  def show
    @member ||= current_user

    @sessions = @member.passwordless_sessions.order(updated_at: :desc).select(
      :remote_addr, :user_agent, :claimed_at
    )

    respond_to_html_json_csv(
      @member,
      V1::MemberSerializer,
      json_serializer_options: {include: [:groups]},
      csv_columns: [:first_name, :last_name, :email, :dob, :id]
    )
  end

  private

  def load_active_members
    defaults = {
      limit: Setting.cached_or_find_by_key(Setting::CONFIGURABLE_LISTING_PER_PAGE),
      offset: 0,
      year: Time.current.year
    }

    @index_params = index_params
    @limit, @offset, @year =
      @index_params
        .with_defaults(defaults)
        .slice(:limit, :offset, :year)
        .values
    @search = @index_params[:search]
    @order = @index_params[:order]
    @years = Membership.select(:year).distinct(:year).order(year: :desc).all

    @active_members = if @year.to_i != 0
      @members.year(@year)
    else
      @members.no_membership
    end
    if @search.present?
      # Bug of pg_search with `.distinct` scope
      # https://github.com/Casecommons/pg_search/issues/238#issuecomment-543702501
      @active_members = @active_members.search(@search).reorder("")
    end
    @page = @offset.to_i / @limit + 1
    @total = @active_members.count
    unless request.format.csv? && index_params[:export] == "all"
      @active_members = @active_members.limit(@limit).offset(@offset)
    end
    @active_members = @active_members.order(@order.to_h) if @order

    # Build families for HTML rendering
    if request.format.html?
      load_families
    end
  end

  def load_families
    members = @active_members.group_by(&:parent_id)
    @families = []
    # Load families with parents first
    parents = members.delete(nil) || []
    parents.each do |parent|
      @families << ([parent] + [members.delete(parent.id)]).flatten.compact
    end
    # Load “orphan” children if any (and fetch their parent /!\ +1 query)
    members.values.each do |children|
      @families << ([children.first.parent] + children).flatten.compact
    end
  end

  def update_params
    if can?(:manage, Member)
      create_params
    else
      params.require(:member).permit(*Member::UPDATE_FIELDS)
    end
  end

  def create_params
    params.require(:member).permit(*Member::CREATION_FIELDS)
  end

  def register_params
    create_params.merge(workflow_state: :candidate)
  end

  def import_params
    params.require(:import_file)
  end

  def index_params
    params.permit(
      :search,
      :autocomplete,
      :offset,
      :year,
      :export,
      order: [:created_at, :last_name, :first_name, :workflow_state]
    )
  end
end
