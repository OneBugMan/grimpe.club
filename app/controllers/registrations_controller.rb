class RegistrationsController < AuthenticatedController
  def index
    defaults = {limit: Setting.cached_or_find_by_key(Setting::CONFIGURABLE_LISTING_PER_PAGE), offset: 0}

    @index_params = index_params
    @limit, @offset =
      @index_params
        .with_defaults(defaults)
        .slice(:limit, :offset)
        .values
    @page = @offset.to_i / @limit + 1
    @order = @index_params[:order]

    @registrations = @registrations.order(@order.to_h) if @order
    @total = @registrations.count
    @registrations = @registrations.limit(@limit).offset(@offset).includes([:creator])

    respond_to_html_json_csv(
      @registrations,
      V1::RegistrationSerializer,
      csv_columns: Registration::CREATION_FIELDS
    )
  end

  def new
  end

  def create
    if @registration.save
      flash[:info] = I18n.t("registrations.create.success")

      redirect_to registrations_path
    else
      if @registration.errors.any?
        flash[:error] = render_activerecord_errors(@registration.errors)
      end

      render :new
    end
  rescue ActiveRecord::StatementInvalid
    flash[:error] = I18n.t("registrations.create.db_error")

    render :new
  end

  def show
  end

  private

  def index_params
    params.permit(
      :offset,
      order: [:created_at, :start_at]
    )
  end

  def create_params
    params
      .require(:registration)
      .permit(*Registration::CREATION_FIELDS)
      .merge(creator: current_user)
  end
end
