class RootController < ApplicationController
  def index
    public_registration_only = current_user.blank? || current_user.candidate?

    @registration = Registration.where(public: public_registration_only).ongoing.first

    @member = current_user || Member.new
  end
end
