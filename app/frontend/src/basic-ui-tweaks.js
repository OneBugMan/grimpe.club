import autoComplete from 'autocomplete';

// Bootstrap JS configuration
document.addEventListener("turbolinks:load", () => {
  // Enable any bootstrap tooltips
  $('[data-toggle="tooltip"]').tooltip();

  $("#autoComplete").each((_idx, elem) => {
    // AutoComplete configuration from input element
    const url = elem.hasAttribute("data-autocomplete") ?
          elem.getAttribute("data-autocomplete") : '/';
    const paramName = elem.hasAttribute("data-autocomplete-param-name") ?
          elem.getAttribute("data-autocomplete-param-name") : 'q';
    const searchAttributeName = elem.hasAttribute("data-autocomplete-search-attr-name") ?
          elem.getAttribute("data-autocomplete-search-attr-name") : 'id';
    const selectAttributeName = elem.hasAttribute("data-autocomplete-select-attr-name") ?
          elem.getAttribute("data-autocomplete-select-attr-name") : 'id';
    const minChars = elem.hasAttribute("data-autocomplete-minchars") ?
          elem.getAttribute("data-autocomplete-minchars") : 3;

    new autoComplete({
      data: {
        src: async () => {
          const input = elem;

          // User search query
          const query = input.value;
          if (query.length < minChars) {
            return {};
          }

          // Fetch External Data Source
          const source = await window.fetch(`${url}?${paramName}=${query}`);
          // Format data into JSON
          const response = await source.json();
          // Return Fetched data (JSON-API format)
          return response.data.map(one => one.attributes);
        },
        key: [searchAttributeName],
        cache: false
      },
      threshold: 2,
      debounce: 300,
      highlight: true,
      searchEngine: (_query, record) => record,
      resultsList: {
        render: true,
        container: source => {
        },
        destination: elem,
        position: "afterend",
        element: "ul"
      },
      resultItem: {
        content: (data, source) => {
          source.setAttribute("class", "autoComplete_result form-control btn btn-outline-secondary");
          source.innerHTML = data.match;
        },
        element: "li"
      },
      onSelection: feedback => {
        const selectInput = $(elem).siblings('.autoComplete_selected');

        selectInput.val(feedback.selection.value[selectAttributeName]);

        return selectInput.parents('form').submit();
      },
    });
  });

  // Auto-dismissed alerts
  $(".alert").delay(9000).slideUp(300, function() {
    $(this).alert('close');
  });
  // Dismiss alerts with keyboard
  $("body").keydown(event => {
    if (event.key === 'Escape') {
      $(".alert").alert('close');
    }
  });

  // Sidebar toggle behavior
  $('#sidebarCollapse').on('click', function() {
    $('#sidebar, #content').toggleClass('active');
  });

  // Enable clickable table rows
  $('table.table-hover tr.clickable-row td').click((evt) => {
    const current = $(evt.currentTarget);
    const target = $(evt.target);
    const clickableRow = current.parent('tr.clickable-row');

    if(target[0] === current[0]) {
      evt.stopPropagation();
      evt.preventDefault();
      evt.stopImmediatePropagation();

      clickableRow.find(':checkbox')[0].click();
    }
  });

  // Support remote GET form submissions with Turbolinks
  // https://github.com/turbolinks/turbolinks-ios/issues/132#issuecomment-481730171
  $('form[method="get"][data-remote="true"]').on("submit", (e) => {
    const url = e.currentTarget.action + '?' + $(e.currentTarget).serialize();

    Turbolinks.visit(url);
  });
});
