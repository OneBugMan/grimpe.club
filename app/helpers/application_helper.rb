# TODO
# Clean these helpers and move them in more comprehensible modules
module ApplicationHelper
  def flash_to_bootstrap_alert(name)
    case name.to_s
    when "error"
      "danger"
    when "alert", "warn"
      "warning"
    else
      name.to_s
    end
  end

  def render_activerecord_errors(errors)
    i18n_error_key = "#{params[:controller].split("/").join(".")}.new.errors"

    <<~ERR
              #{I18n.t!(i18n_error_key, errors: ActionController::Base.helpers.pluralize(errors.count, t("error")))}
      <ul>
        <li>#{errors.full_messages.join("</li><li>")}</li>
      </ul>
    ERR
  rescue I18n::MissingTranslationData
    <<~ERR
      <ul>
        <li>#{errors.full_messages.join("</li><li>")}</li>
      </ul>
    ERR
  end

  def order_link(attribute, order: {})
    classnames = ["btn"]
    arrow = "↑"
    opposite_direction = "DESC"

    if order && order[attribute]
      opposite_direction = order[attribute].downcase.to_s == "desc" ? "ASC" : "DESC"
      arrow = opposite_direction == "ASC" ? "↓" : arrow
    end

    link_to arrow,
      request.query_parameters.merge(order: {attribute => opposite_direction}),
      class: classnames.join(" ")
  end

  def render_member_state_bootstrap_badge(state)
    text = t("activerecord.attributes.member.workflow_states.#{state}")
    color = case state.to_s
            when "expired"
              "secondary"
            when "registered"
              "success"
            when "selected"
              "warning"
            when "candidate"
              "info"
    end
    "<span class='badge badge-#{color}'>#{text}</span>"
  end

  def emoji_sexe(sexe)
    if sexe.to_s == Member::FEMALE.to_s
      "👩"
    elsif sexe.to_s == Member::MALE.to_s
      "👨"
    else
      "👩👨"
    end
  end

  def club_name
    Setting.cached_or_find_by_key(Setting::CONFIGURABLE_CLUB_NAME)
  end

  private

  def current_controller?(names)
    names.include?("/#{params[:controller]}")
  end
end
