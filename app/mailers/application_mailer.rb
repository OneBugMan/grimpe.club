class ApplicationMailer < ActionMailer::Base
  helper ApplicationHelper

  default from: "noreply@grimpe.club"
  layout "mailer"
end
