# == Schema Information
#
# Table name: members
#
#  id             :uuid             not null, primary key
#  address        :text             not null
#  city           :string           not null
#  dob            :date             not null
#  email          :string           not null
#  first_name     :string           not null
#  last_name      :string           not null
#  phone          :string
#  role           :integer          not null
#  sexe           :integer          not null
#  workflow_state :string           not null
#  zipcode        :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  parent_id      :uuid
#
# Indexes
#
#  index_members_on_email                                    (email) UNIQUE
#  index_members_on_f_unaccent_first_name_text_gin_trgm_ops  (f_unaccent((first_name)::text) gin_trgm_ops) USING gin
#  index_members_on_f_unaccent_last_name_text_gin_trgm_ops   (f_unaccent((last_name)::text) gin_trgm_ops) USING gin
#  index_members_on_parent_id                                (parent_id)
#
class Member < ApplicationRecord
  FEMALE = :female
  MALE = :male
  SEXES = [FEMALE, MALE]

  ROLE_ADMIN = :admin
  ROLE_BASIC = :basic
  ROLES = [ROLE_ADMIN, ROLE_BASIC]

  IMPORT_FIELDS = [
    :first_name,
    :last_name,
    :email,
    :dob,
    :sexe,
    :address,
    :city,
    :zipcode,
    :phone,
    :workflow_state,
    "memberships.year",
    :parent_id
  ]

  EXPORT_FIELDS = IMPORT_FIELDS + [
    :id
  ]

  UPDATE_FIELDS = [
    :phone,
    :address, :zipcode, :city
  ]
  CREATION_FIELDS = UPDATE_FIELDS + [
    :first_name, :last_name, :dob, :sexe,
    :email,
    :parent_id, :workflow_state
  ]

  enum sexe: SEXES
  enum role: ROLES

  # Member model relationships
  has_many :memberships, dependent: :destroy
  has_many :groups, through: :memberships
  has_many :current_groups, -> {
    current.distinct
  }, through: :memberships, source: :group

  belongs_to :parent, class_name: "Member", optional: true
  has_many :children, class_name: "Member", foreign_key: "parent_id", inverse_of: :parent, dependent: :nullify

  # Member model validations and initialisations
  validates :email, format: {with: URI::MailTo::EMAIL_REGEXP}, presence: true, uniqueness: {case_sensitive: false}
  validates :dob, :sexe, :address, :zipcode, :city, presence: true
  before_create :build_with_defaults

  # Member query interfaces (scopes)
  scope :current, -> { year(Time.current.year) }
  scope :current_children, -> { current.select("children_members.*").left_outer_joins(:children) }
  scope :year, ->(year) { joins(:memberships).merge(Membership.only_year(year)).distinct }
  scope :no_membership, -> { left_joins(:memberships).where(memberships: {id: nil}).distinct }

  # Authentication mechanism via 'Password-less' logins
  passwordless_with :email

  # Member model workflow (state and transitions)
  include WorkflowActiverecord
  workflow do
    state :candidate do
      event :draw, transitions_to: :selected
    end
    state :selected do
      event :subscribe, transitions_to: :registered, if: :valid_subscription?
    end
    state :expired do
      event :renewal, transitions_to: :registered, if: :valid_subscription?
    end
    state :registered do
      event :expire, transitions_to: :expired
    end
  end

  # Full text search capabilities
  include PgSearch::Model
  pg_search_scope :search,
    against: [:first_name, :last_name],
    ignoring: :accents,
    using: {
      trigram: {word_similarity: true}
    }

  public

  def active?
    self.class.current.where(id: id).present?
  end

  def latest_membership
    memberships.order(:year).last
  end

  def age
    ActiveSupport::Duration.build(Time.zone.now - dob.to_time).parts[:years]
  end

  def name
    "#{first_name} #{last_name}"
  end

  def admin?
    role.to_s == ROLE_ADMIN.to_s
  end

  def to_s
    "#<#{self.class}:#{id} state=#{workflow_state} name=#{name}>"
  end

  def self.create_from_raw!(raw_attributes)
    attributes = raw_attributes.symbolize_keys
    member = attributes.slice(*IMPORT_FIELDS)

    if member[:sexe].present? && SEXES.exclude?(member[:sexe].to_sym)
      member[:sexe] = SEXES[member[:sexe].to_i]
    end

    if attributes[:year].present?
      member[:memberships] = [
        Membership.new({group: Group.default}.merge(attributes.extract!(:year)))
      ]
    end

    create!(member)
  end

  private

  # TODO
  def valid_subscription?
    true
  end

  def build_with_defaults
    # All members have basic role by default
    self.role = Member::ROLE_BASIC
    self.workflow_state ||= :candidate
  end
end
