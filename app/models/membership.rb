# == Schema Information
#
# Table name: memberships
#
#  id         :uuid             not null, primary key
#  year       :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  group_id   :uuid             not null
#  member_id  :uuid             not null
#
# Indexes
#
#  index_memberships_on_group_id                         (group_id)
#  index_memberships_on_member_id                        (member_id)
#  index_memberships_on_member_id_and_group_id_and_year  (member_id,group_id,year) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (group_id => groups.id)
#  fk_rails_...  (member_id => members.id)
#
class Membership < ApplicationRecord
  belongs_to :member
  belongs_to :group

  validate if: :with_member? do |m|
    unless m.member.registered? || m.group.is_default?
      m.errors[:member] << I18n.t(
        "activerecord.errors.membership.not_registered",
        state: m.member.workflow_state
      )
    end
  end

  scope :current, -> {
    only_year(Time.current.year)
  }
  scope :only_year, ->(year) {
    where(year: year)
  }

  def human_year
    "#{year - 1} / #{year}"
  end

  private

  def with_member?
    member.present?
  end
end
