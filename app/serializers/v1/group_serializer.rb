class V1::GroupSerializer
  include FastJsonapi::ObjectSerializer

  # Private attributes
  attributes :name,
    :description,
    :email
end
