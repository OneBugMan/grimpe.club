class V1::MemberSerializer
  include FastJsonapi::ObjectSerializer

  has_many :groups, serializer: V1::GroupSerializer

  # Private attributes
  attributes :id,
    :first_name,
    :last_name,
    :email,
    :dob, :age
end
