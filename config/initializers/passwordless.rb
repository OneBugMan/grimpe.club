# Redirect to destination page after signing in
Passwordless.redirect_back_after_sign_in = true

# Magic link valid only once
Passwordless.restrict_token_reuse = true

# How long until a magic link expires
Passwordless.timeout_at = lambda { 5.minutes.from_now }

Passwordless.parent_mailer = "ApplicationMailer"
