// use the new NPM package name, `shakapacker`.
// merge is webpack-merge from https://github.com/survivejs/webpack-merge
const { webpackConfig: baseWebpackConfig, merge } = require('shakapacker')
const webpack = require('webpack');

// Copy the object using merge b/c the baseClientWebpackConfig is a mutable global
// If you want to use this object for client and server rendering configurations,
// havaing a new object is essential.
module.exports = merge({}, baseWebpackConfig, {
  plugins: [
    // 'Provide',
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      Popper: ['popper.js', 'default'],
      RailsUJS: ['@rails/ujs'],
      Turbolinks: ['turbolinks']
    })
  ],
  resolve: {
    alias: {
      jquery: 'jquery/src/jquery',
      autocomplete: '@tarekraafat/autocomplete.js/dist/js/autoComplete.min'
    },
    extensions: [
      '.css', '.ts', '.tsx', '.js', '.jsx', '.scss', '.png', '.svg', '.jpg', '.jpeg'
    ]
  }
})
