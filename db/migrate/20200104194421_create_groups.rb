class CreateGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :groups, id: :uuid do |t|
      t.string :name, null: false, index: {unique: true}
      t.string :description
      t.string :email

      t.timestamps
    end
  end
end
