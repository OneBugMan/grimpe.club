class CreateRegistrations < ActiveRecord::Migration[6.0]
  def change
    create_table :registrations, id: :uuid do |t|
      t.date :start_at, null: false
      t.date :end_at, null: false
      t.integer :quantity, null: true
      t.boolean :public, null: false, default: false
      t.references :creator, references: :members, type: :uuid, null: false, index: true

      t.timestamps
    end

    reversible do |dir|
      dir.up do
        execute <<-SQL.squish
          ALTER TABLE registrations
            ADD CONSTRAINT non_empty_period
              CHECK (start_at < end_at);
          ALTER TABLE registrations
            ADD CONSTRAINT overlapping_dates
              EXCLUDE USING GIST (
                (public::int) WITH =,
                TSRANGE(start_at, end_at) WITH &&
              );
        SQL
      end
      dir.down do
        execute <<-SQL.squish
          ALTER TABLE registrations
            DROP CONSTRAINT non_empty_period;
          ALTER TABLE registrations
            DROP CONSTRAINT overlapping_dates;
        SQL
      end
    end
  end
end
