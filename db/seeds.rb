# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#
#
# We use test fixtures for seeding our development database
unless Rails.env.production? || Member.count.positive?
  Rails.application.load_tasks
  ENV["FIXTURES"] = "groups,members,memberships,settings"
  Rails.logger.info "Loading all fixtures... [#{ENV["FIXTURES"]}]"
  Rake::Task["db:fixtures:load"].execute
  Rails.logger.info "All fixtures loadded ✓"
end
