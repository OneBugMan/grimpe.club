namespace :admin do
  desc "Create an administrator member"
  task :create, [:email] => [:environment] do |task, args|
    if args.email.blank?
      puts <<~MSG
        Please provide an email as argument to this task
        > rake #{task.name}['john@grimpe.club']"
      MSG
      exit 1
    end

    Member.create!(
      email: args.email,
      first_name: "ChangeMe",
      last_name: "ChangeMe",
      dob: Time.current,
      sexe: 0,
      role: Member::ROLE_ADMIN,
      city: "ChangeMe",
      address: "ChangeMe",
      zipcode: "ChangeMe"
    )
  end

  desc "Promote member to administrator"
  task :update, [:email] => [:environment] do |task, args|
    if args.email.blank?
      puts <<~MSG
        Please provide an email as argument to this task
        > rake #{task.name}['john@grimpe.club']"
      MSG
      exit 1
    end

    member = Member.find_by(email: args.email)

    if member.blank?
      puts <<~MSG
        Member with email '#{args.email}' does not exist.
        Please provide an existing member's email.
      MSG
      exit 1
    end

    puts <<~MSG
      Promoting member '#{args.email}' to admin…
    MSG

    if member.role.to_s != Member::ROLE_ADMIN.to_s
      member.update(role: Member::ROLE_ADMIN)
    else
      warn <<~MSG
        WARNING: member is already an admin
      MSG
    end
    puts <<~MSG
      Done ✓
    MSG
  end
end
