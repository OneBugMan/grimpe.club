#!/usr/bin/env bash

# Exit code is a 'mask' of found severities
# cf https://classic.yarnpkg.com/en/docs/cli/audit/
yarn audit || [ "$?" -lt "8" ]
