require "test_helper"

class RegistrationsControllerTest < ActionDispatch::IntegrationTest
  describe "Unlogged member" do
    test "should not be able to access any registration features" do
      get registrations_url
      assert_response :redirect
      assert_redirected_to send(Passwordless.mounted_as).sign_in_path

      get new_registration_url
      assert_response :redirect
      assert_redirected_to send(Passwordless.mounted_as).sign_in_path

      post registrations_url
      assert_response :redirect
      assert_redirected_to send(Passwordless.mounted_as).sign_in_path
    end
  end

  describe "Logged in as an admin member" do
    test "should get index" do
      sign_in_as(members(:janja)) do
        get registrations_url
        assert_response :success
      end
    end

    test "should get new form" do
      sign_in_as(members(:janja)) do
        get new_registration_url
        assert_response :success
      end
    end

    test "should be able to create" do
      sign_in_as(members(:janja)) do
        assert_difference("Registration.count") do
          post registrations_url,
            params: {registration: {start_at: Time.current, end_at: 1.week.from_now}}
        end
        reg = Registration.order(created_at: :desc).first
        assert_redirected_to registrations_path
        assert_equal members(:janja), reg.creator
      end
    end
  end
end
