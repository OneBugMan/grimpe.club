FactoryBot.define do
  factory :registration do
    start_at { 1.day.from_now }
    end_at { 3.days.from_now }
    creator { build(:member) }

    trait :ongoing do
      start_at { 1.day.ago }
    end
  end

  factory :member do
    first_name { "Jean-Michel" }
    last_name { "Cambon" }
    email { "jm.cambon@grimpe.club" }
    dob { "05-01-1952" }
    sexe { "male" }
    address { "Prey d'aval" }
    zipcode { "05100" }
    city { "Puy-Daint-Vincent" }
  end

  factory :group do
    name { "Ski de randonnée" }
    description { "C'est le ski de randonnée" }
  end
end
