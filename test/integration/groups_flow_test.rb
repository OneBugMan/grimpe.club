require "test_helper"

class GroupsFlowTest < ActionDispatch::IntegrationTest
  test "Signed in as an admin can see & edit all groups" do
    sign_in_as(members(:janja)) do
      get groups_path
      assert_response :success
      assert_select "tr.clickable-row", Group.count

      # Edit group details page
      Group.all.each do |group|
        get edit_group_path(group)
        assert_response :success
      end

      # Add & remove members from a group
      mur = groups(:mur)
      current_member_count = mur.current_members.count
      get group_path(mur)
      assert_response :success
      assert_select "li.list-group-item.member", current_member_count

      post group_memberships_path(mur), params: {membership: {member_id: members(:cedric).id}}
      assert_nil flash[:error]
      assert_response :redirect
      follow_redirect!
      assert_select "li.list-group-item.member", current_member_count + 1

      delete group_membership_path(mur, Membership.order(created_at: :desc).first)
      assert_nil flash[:error]
      assert_response :redirect
      follow_redirect!
      assert_select "li.list-group-item.member", current_member_count

      # Can create a group
      new_group = groups(:mur)
      new_group.email = ""
      new_group.name = "#{new_group.name}2"
      assert_changes -> { Group.count }, from: Group.count, to: Group.count + 1 do
        post groups_path, params: {group: new_group.attributes}
        assert_response :redirect
        assert_nil flash[:error]
      end

      # Can update the new group
      new_group = Group.order(created_at: :desc).first
      expected_new_email = "group@example.org"
      assert_changes -> { new_group.reload.email }, from: "", to: expected_new_email do
        patch group_path(new_group.reload), params: {group: {email: expected_new_email}}
      end
    end
  end

  test "Signed in member can view all groups but edit only its own group" do
    cedric = members(:cedric)

    sign_in_as(cedric) do
      # List groups
      get groups_path
      assert_response :success
      # Should see only its own groups
      assert_select "tr.clickable-row", Group.current.count

      my_group = cedric.groups.first

      # Show group page
      get group_path(my_group)
      assert_response :success

      # Edit group details
      get edit_group_path(my_group)
      assert_response :success

      # Cannot edit other groups' details
      not_my_group = members(:janja).groups.first
      get edit_group_path(not_my_group)
      assert_response :redirect
      follow_redirect!
      assert_equal "You are not authorized to access this page.", flash[:warning]

      # Cannot add members to the group
      post group_memberships_path(my_group),
        params: {membership: {member_id: members(:janja).id}}
      assert_response :redirect
      follow_redirect!
      assert_equal "You are not authorized to access this page.", flash[:warning]
    end
  end

  test "Can import groups" do
    sign_in_as(members(:janja)) do
      post import_groups_path,
        params: {import_file: fixture_file_upload("2-groups.csv", "text/csv")}
      assert_equal I18n.t("groups.import.success", created: 2), flash[:info]
      assert_response :redirect
      follow_redirect!
    end
  end
end
