require "test_helper"

class MembersFlowTest < ActionDispatch::IntegrationTest
  describe Member, :admin do
    test "can manage all members" do
      sign_in_as(members(:janja)) do
        get members_path
        assert_response :success
        assert_select "tr.clickable-row.member-row", Member.current.count

        # Previous year members
        previous_year = Time.current.year - 1
        get members_path(year: previous_year)
        assert_response :success
        assert_select "tr.clickable-row.member-row", Member.year(previous_year).count
      end
    end

    test "Can create members" do
      sign_in_as(members(:janja)) do
        new_member = members(:cedric)
        new_member.email = "#{new_member.email}.com"
        assert_changes -> { Member.count }, from: Member.count, to: Member.count + 1 do
          post members_path, params: {member: new_member.attributes}
          assert_nil flash[:error]
          assert_response :redirect
          follow_redirect!

          post members_path, params: {member: new_member.attributes}
          assert_response :success
          assert_match I18n.t("errors.messages.taken"), flash[:error]
        end
      end
    end

    test "Can export members" do
      sign_in_as(members(:janja)) do
        # Export current page
        offset = 5
        get "#{members_path}?format=csv&offset=#{offset}"
        assert_response :success
        assert_equal "text/csv", response.media_type
        lines = response.body.split("\n")
        headers = lines.shift
        expected_fields = Member::EXPORT_FIELDS.map do |field|
          field.to_s.split(".").last
        end
        assert_equal expected_fields, headers.split(",")
        assert_equal (Member.current.count - offset), lines.count

        # Export all members
        get "#{members_path}?format=csv&offset=#{offset}&export=all"
        assert_response :success
        assert_equal "text/csv", response.media_type
        lines = response.body.split("\n")
        lines.shift # headers
        assert_equal Member.current.count, lines.count
      end
    end

    test "Can import members" do
      sign_in_as(members(:janja)) do
        post import_members_path,
          params: {import_file: fixture_file_upload("3-members.csv", "text/csv")}
        assert_equal I18n.t("members.import.success", created: 3), flash[:info]
        assert_response :redirect
        follow_redirect!
      end
    end
  end

  describe Member, :basic do
    test "an old user can still see & edit itself" do
      old = members(:sebastien)
      sign_in_as(old) do
        get edit_member_path(old)
        assert_response :success

        assert_equal "Sébastien", old.first_name
        new_phone = "0100221122"
        patch member_path(old), params: {member: {phone: new_phone}}
        assert_equal I18n.t("members.update.success"), flash[:info]
        assert_response :redirect
        assert_equal new_phone, old.reload.phone
        follow_redirect!
        # But can't edit someone else
        not_me = members(:cedric)
        patch member_path(not_me), params: {member: {phone: new_phone}}
        assert_equal "You are not authorized to access this page.", flash[:warning]
        assert_response :redirect
        follow_redirect!
        assert_not_equal new_phone, not_me.reload.phone
      end
    end

    test "current member can view and edit itself" do
      cedric = members(:cedric)

      sign_in_as(cedric) do
        # Can't list members
        get members_path
        assert_equal "You are not authorized to access this page.", flash[:warning]
        assert_response :redirect
        follow_redirect!

        # Show self member page
        get member_path(cedric)
        assert_response :success

        # Edit member details
        get edit_member_path(cedric)
        assert_response :success

        # Cannot edit other members' details
        get edit_member_path(members(:janja))
        assert_equal "You are not authorized to access this page.", flash[:warning]
        assert_response :redirect
        follow_redirect!
      end
    end

    test "Can not import members" do
      sign_in_as(members(:cedric)) do
        post import_members_path,
          params: {import_file: fixture_file_upload("3-members.csv", "text/csv")}
        assert_equal "You are not authorized to access this page.", flash[:warning]
        assert_response :redirect
        follow_redirect!
      end
    end
  end

  describe Member, :candidate do
    test "Candidate members can't see any members" do
      patrick = members(:patrick)

      sign_in_as(patrick) do
        # Can't list members
        get members_path
        assert_equal "You are not authorized to access this page.", flash[:warning]
        assert_response :redirect
        follow_redirect!
      end
    end

    test "Candidate members can edit their info" do
      patrick = members(:patrick)

      sign_in_as(patrick) do
        # Can't list members
        get members_path
        assert_equal "You are not authorized to access this page.", flash[:warning]
        assert_response :redirect
        follow_redirect!

        # Edit own member details
        get edit_member_path(patrick)
        assert_nil flash[:error]
        assert_response :success

        new_phone = "0677112211"
        patch member_path(patrick), params: {member: {phone: new_phone}}
        assert_equal I18n.t("members.update.success"), flash[:info]
        assert_response :redirect
        assert_nil flash[:error]
        follow_redirect!
        assert_equal new_phone, patrick.reload.phone

        # Can't change his state himself
        assert_equal "candidate", patrick.workflow_state
        patch member_path(patrick), params: {member: {workflow_state: :registered}}
        assert_empty flash
        assert_equal "candidate", patrick.reload.workflow_state

        # Can't add memberships neither
        post group_memberships_path(Group.default),
          params: {membership: {member_id: patrick.id}}
        assert_response :redirect
        follow_redirect!
        assert_equal "You are not authorized to access this page.", flash[:warning]

        # Cannot edit other members' details
        get edit_member_path(members(:janja))
        assert_equal "You are not authorized to access this page.", flash[:warning]
        assert_response :redirect
        follow_redirect!
      end
    end
  end
end
