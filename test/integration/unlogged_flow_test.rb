require "test_helper"

class UnloggedFlowTest < ActionDispatch::IntegrationTest
  test "can visit the homepage" do
    get root_path
    assert_response :success
    assert_select "h1", "Welcome on Grimpe.club"
  end

  test "can sign-in" do
    # Trying to visit members page redirects to sign in page
    get members_path
    assert_redirected_to send(Passwordless.mounted_as).sign_in_path
    follow_redirect!
    assert_response :success
    assert_select "h1", I18n.t("members.sign_in.title")
    assert_select "input#passwordless_email"

    # Sign in
    sign_in_as(members(:sebastien)) do
      get root_path
      assert_response :success
    end
  end

  test "can submit an application to a public registration" do
    get root_path

    assert_response :success
    assert_select "h2", "Registration period of the club is open!"
    assert_select "form"

    new_member_info = {
      first_name: "Thomas",
      last_name: "Crecre",
      sexe: "male",
      dob: Date.parse("1991-09-01"),
      email: "thomas.c@grimpe.club",
      phone: "+33102030405",
      address: "123 rue du plop",
      zipcode: "79001",
      city: "VilleTown"
    }

    candidates = Member.where(workflow_state: "candidate")

    assert_changes -> { candidates.count }, from: candidates.count, to: candidates.count + 1 do
      post register_members_path, params: {member: new_member_info}
      assert_response :success

      assert_select "h5", I18n.t("unlogged_index.registration_saved")
    end
  end
end
