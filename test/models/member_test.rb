require "test_helper"

class MemberTest < ActiveSupport::TestCase
  test "John & Marie are members" do
    members = members(:cedric, :janja)

    assert_equal [:male.to_s, :female.to_s], members.pluck(:sexe)
    assert_equal [false, true], members.map(&:admin?)
  end

  test "Kiddo should have a parent" do
    kid = members(:kid)

    assert members.any? { |m| m == kid.parent }, "Kid #{kid.first_name} should have a parent"
  end

  test "Members should be searchable" do
    [
      ["janj", [members(:janja)]],
      ["hris", [members(:chris)]],
      ["seb", [members(:sebastien)]]
    ].each do |(term, members)|
      searching = Member.search(term)
      assert_equal members.count, searching.count, "Searching for '#{term}' should find one member"
      assert_equal members.map(&:first_name).sort, searching.pluck(:first_name).sort
    end
  end

  test "Creating a new member" do
    member = Member.create(
      members(:sebastien).attributes.reject { |k, v| ["id", "email"].include?(k) }.merge(
        email: "new@new.org"
      )
    )

    assert member.save, member.errors.full_messages
  end

  test "Current members should be this years' membership" do
    current_members_count = Member.current.count
    current_memberships_count = Membership.current.where(group: Group.default).count

    assert_equal current_memberships_count, current_members_count, "Expecting #{current_memberships_count} 'current' members and got #{current_members_count}"
  end
end
