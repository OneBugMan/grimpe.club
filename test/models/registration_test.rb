require "test_helper"

class RegistrationTest < ActiveSupport::TestCase
  test "validations" do
    reg = Registration.create(start_at: 2.days.ago, end_at: 1.day.ago, creator: Member.last)
    reg.save

    assert reg.errors.present?
    assert_match(I18n.t("activerecord.errors.registration.no_past"), reg.errors.full_messages.first)

    reg = Registration.create(start_at: 2.days.from_now, end_at: 1.day.from_now, creator: Member.last)
    reg.save

    assert reg.errors.present?
    assert_match(I18n.t("activerecord.errors.registration.after_end"), reg.errors.full_messages.first)
  end

  test "::ongoing scope" do
    assert_equal 1, Registration.ongoing.count

    registration = create(:registration, :ongoing)

    assert_equal 2, Registration.ongoing.count

    registration.update(end_at: DateTime.current)

    assert_equal 2, Registration.ongoing.count
  end
end
