if ENV.fetch("COVERAGE", true)
  require "simplecov"
end
ENV["RAILS_ENV"] ||= "test"
require_relative "../config/environment"
require "rails/test_help"
require "minitest/spec"
Minitest::Reporters.use! [
  Minitest::Reporters::DefaultReporter.new(color: true),
  Minitest::Reporters::JUnitReporter.new
]

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  extend MiniTest::Spec::DSL

  # Use factoryBot
  include FactoryBot::Syntax::Methods
end

module SigninHelper
  def sign_in_as(user)
    session = Passwordless::Session.create!(
      authenticatable: user,
      user_agent: "rails integration test",
      remote_addr: "coucou"
    )

    get Passwordless::Engine.routes.url_helpers.token_sign_in_path(session.token)
    assert_response :redirect
    follow_redirect!

    yield

    # sign out
    get Passwordless::Engine.routes.url_helpers.sign_out_path
    assert_response :redirect
    follow_redirect!
  end
end

class ActionDispatch::IntegrationTest
  include SigninHelper
end
